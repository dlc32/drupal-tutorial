FROM bitnami/drupal:latest

USER 0

WORKDIR /opt/bitnami/drupal
RUN composer require 'drupal/field_permissions:^1.2'

WORKDIR /opt/bitnami/drupal/modules/contrib
COPY /modules/colldev /opt/bitnami/drupal/modules/contrib/

ENV DRUPAL_ENABLE_MODULES "colldev"

WORKDIR /opt/bitnami/drupal
RUN chmod -R a+rwx /opt/bitnami/drupal/modules

USER 1001

ENV SHELL /bin/bash
