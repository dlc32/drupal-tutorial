<?php

namespace Drupal\colldev\Commands;

use Drush\Commands\DrushCommands;
use Drush\Drush;
use Symfony\Component\Yaml\Yaml;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;

/**
 * A Drush commandfile
 *
 */
class ColldevCommands extends DrushCommands {
  /**
   * Import data from remote colldev server.
   *
   * @param $hostname
   *   Collections Development host from which to query
   * @param array $options
   *   An associative array of options from CLI
   * @usage colldev-remoteImpomrt
   *
   * @command colldev:remoteImport
   * @aliases remimp
   */
  public function remoteImport($hostname, $options = ['content-type' => 'purchase_suggestion']) {
    $node = Node::create(['type' => 'purchase_suggestion']);
    $node->langcode = 'en';
    $node->body = 'Here we go with a new suggestion';
    $node->promote = 0;
    $node->uid = 1;
    $node->title = 'New Suggestion for 2023';
    $node->field_item_authors = 'Me and friends';
    $node->save();
  }
}
