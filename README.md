# DUL Drupal Tutorial
[[_TOC_]]
## Before You Get Started...
It is assumed that you have `docker-compose` installed.  

### Create Local "Volume" Directory
Follow the steps below in order to persist the application -- in the event you need to bring the stack down.  
  
```sh
$ cd /path/to/this/project

$ mkdir drupal-data
$ sudo chown 1000:root drupal-data
$ sudo chmod a+rwx drupal-data
```
  
## The First Run
```sh
$ docker-compose up --build
```

The image, based on `docker.io/bitnami/drupal:latest` will build and include the `colldev` 
module located at `modules/colldev` in this repository.  
  
After a successful build, the Drupal application will initialize.  
The example module, `colldev`, will be enabled during an initial bootstrapping process.  
  
More on the module below.
  
At the end of the startup process, you'll see this in your console:  
```sh
drupal     | drupal 20:40:46.68 INFO  ==> Installing Drupal site
drupal     | 
drupal     |  You are about to:
drupal     |  * DROP all tables in your 'bitnami_drupal' database.
drupal     | 
drupal     |  // Do you want to continue?: yes.                                              
drupal     | 
drupal     |  [notice] Starting Drupal installation. This takes a while.
drupal     |  [notice] Performed install task: install_select_language
drupal     |  [notice] Performed install task: install_select_profile
drupal     |  [notice] Performed install task: install_load_profile
drupal     |  [notice] Performed install task: install_verify_requirements
drupal     |  [notice] Performed install task: install_settings_form
drupal     |  [notice] Performed install task: install_verify_database_ready
drupal     |  [notice] Performed install task: install_base_system
drupal     |  [notice] Performed install task: install_bootstrap_full
drupal     |  [notice] Performed install task: install_profile_modules
drupal     |  [notice] Performed install task: install_profile_themes
drupal     |  [notice] Performed install task: install_install_profile
drupal     |  [notice] Performed install task: install_configure_form
drupal     |  [notice] Performed install task: install_finished
drupal     |  [success] Installation complete.
drupal     | drupal 20:40:55.62 INFO  ==> Enabling Drupal modules
drupal     |  [success] Successfully enabled: field_permissions, colldev
drupal     | drupal 20:41:00.87 INFO  ==> Flushing Drupal cache
drupal     |  [success] Cache rebuild complete.
drupal     | drupal 20:41:02.25 INFO  ==> Persisting Drupal installation
drupal     | 
drupal     | drupal 20:41:02.36 INFO  ==> ** Drupal setup finished! **
drupal     | drupal 20:41:02.37 INFO  ==> ** Starting Apache **
drupal     | [Thu Jan 19 20:41:02.410692 2023] [ssl:warn] [pid 1] AH01909: www.example.com:8443:0 server certificate does NOT include an ID which matches the server name
drupal     | [Thu Jan 19 20:41:02.411031 2023] [ssl:warn] [pid 1] AH01909: www.example.com:443:0 server certificate does NOT include an ID which matches the server name
drupal     | [Thu Jan 19 20:41:02.428998 2023] [ssl:warn] [pid 1] AH01909: www.example.com:8443:0 server certificate does NOT include an ID which matches the server name
drupal     | [Thu Jan 19 20:41:02.429310 2023] [ssl:warn] [pid 1] AH01909: www.example.com:443:0 server certificate does NOT include an ID which matches the server name
drupal     | [Thu Jan 19 20:41:02.439040 2023] [mpm_prefork:notice] [pid 1] AH00163: Apache/2.4.55 (Unix) OpenSSL/1.1.1n configured -- resuming normal operations
drupal     | [Thu Jan 19 20:41:02.439059 2023] [core:notice] [pid 1] AH00094: Command line: '/opt/bitnami/apache/bin/httpd -f /opt/bitnami/apache/conf/httpd.conf -D FOREGROUND'
```
  
Visit http://localhost:8080 to see the Drupal instance in action.  
  
### Logging In
The username is `user`, and the password is `bitnami`.  
This user has full admin rights.
  

## Stopping The Application
Press `ctrl-c` to stop the application. Then...  
  
```sh
$ docker-compose down
```
  
### If You Want To Reset Everything...
```sh
$ sudo rm -rf /path/to/this/project/drupal-data/*

$ drupal volume prune -f      # (optional -- removes the mariadb volume)
$ drupal system prune -af     # (if all else fails)
```
## The Example Module: Colldev

The purpose of this example module is to:
* creating a custom "content type" using a configuration file located in the module's `config/install` directory,
* creating fields and attaching to the new content type, also using configuration files located in the `config/install` directory,
* how to programmatically create a node when the module is enabled,
* how to create a Drush command for the example module
  

