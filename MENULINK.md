```php
<?php
use Drupal\menu_link_content\Entity\MenuLinkContent;
// other use Drupal\... here

function some_module_install() {
    $menu_name = 'main';

    $link_uri = 'internal:/recommendations';
    $link_title = 'Recommendations';
    
    $edit = [
        'title' => $l['link_title'],
        'link' => ['uri' => $link_uri],
        'menu_name' => $menu_name,
        'weight' => 0,
        'expanded' => $expanded,
        'external' => 0,
        'enabled' => 0,
    ];

    }
    $menu_link = MenuLinkContent::create($edit);
    $menu_link->save();
}
